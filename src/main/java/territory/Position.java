package territory;

import com.google.gson.annotations.SerializedName;
import messages.BoardResponseMessage;

import java.util.Objects;

public final class Position {

	@SerializedName("x")
	private final int row;

	@SerializedName("y")
	private final int column;

	public Position(int row, int column) {
		this.row = row;
		this.column = column;
	}

	/**
	 * Move into the direction specified.
	 * @param direction The direction which to move to
	 * @return The new position which the direction specified
	 * @throws InvalidMoveException The move cannot be made.
	 */
	public Position move(Move direction) throws InvalidMoveException {
		switch (direction) {
			case UP:
				return this.up();
			case DOWN:
				return this.down();
			case LEFT:
				return this.left();
			case RIGHT:
				return this.right();
		}
		// The return statement is here since Java thinks it needs a return type.
		// This should never be possible since the switch statement covers all possible values
		return null;
	}

	/**
	 * Move up.
	 * @return The position up from the current position.
	 * @throws InvalidMoveException The move cannot be made.
	 */
	public Position up() throws InvalidMoveException {
		if (row > 0) {
			return new Position(row - 1, column);
		}
		throw new InvalidMoveException(Move.UP, this);
	}

	/**
	 * Move down.
	 * @return The position down from the current position.
	 * @throws InvalidMoveException The move cannot be made.
	 */
	public Position down() throws InvalidMoveException {
		if (row < Board.ROWS - 1) {
			return new Position(row + 1, column);
		}
		throw new InvalidMoveException(Move.DOWN, this);
	}

	/**
	 * Move left.
	 * @return The position left from the current position.
	 * @throws InvalidMoveException The move cannot be made.
	 */
	public Position left() throws InvalidMoveException {
		if (column > 0) {
			return new Position(row, column - 1);
		}
		throw new InvalidMoveException(Move.LEFT, this);
	}

	/**
	 * Move right.
	 * @return The position right from the current position.
	 * @throws InvalidMoveException The move cannot be made.
	 */
	public Position right() throws InvalidMoveException {
		if (column < Board.COLUMNS - 1) {
			return new Position(row, column + 1);
		}
		throw new InvalidMoveException(Move.RIGHT, this);
	}

	/**
	 * Get the X-coordinate or the row of this position.
	 * @return The X-coordinate or row of this position.
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Get the Y-coordinate or the row of this position.
	 * @return The Y-coordinate or row of this position.
	 */
	public int getColumn() {
		return column;
	}

	@Override
	public String toString() {
		return "Position(" + row + ", " + column + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Position position = (Position) o;
		return row == position.row &&
				column == position.column;
	}

	@Override
	public int hashCode() {
		return Objects.hash(row, column);
	}
}
