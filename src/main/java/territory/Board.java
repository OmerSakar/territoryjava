package territory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board {

	public static final int ROWS = 5;
	public static final int COLUMNS = 5;

	private int[][] places;

	private Position positionOne;
	private Position positionTwo;

	/**
	 * Create an empty board.
	 */
	public Board() {
		this.places = new int[ROWS][COLUMNS];
		this.places[0][0] = 1;
		this.places[ROWS - 1][COLUMNS - 1] = 2;

		this.positionOne = new Position(0, 0);
		this.positionTwo = new Position(Board.ROWS - 1, Board.COLUMNS - 1);
	}

	/**
	 * Create a board with the specified places and player positions.
	 *
	 * @param places    The current state of the board.
	 * @param playerOne The position of player one.
	 * @param playerTwo The position of player two.
	 */
	public Board(int[][] places, Position playerOne, Position playerTwo) {
		this.places = places;
		this.positionOne = playerOne;
		this.positionTwo = playerTwo;
	}

	public int getPlace(int row, int column) {
		return places[row][column];
	}

	public int[][] getPlaces() {
		return places;
	}

	private void setPlace(Position position, int playerNumber) {
		this.places[position.getRow()][position.getColumn()] = playerNumber;
	}

	public Position getPositionOne() {
		return positionOne;
	}

	/**
	 * Set the new position of player one and fill in that position on the board.
	 *
	 * @param positionOne The new position to fill.
	 */
	public void setPositionOne(Position positionOne) {
		this.positionOne = positionOne;
		setPlace(this.positionOne, 1);
	}

	public Position getPositionTwo() {
		return positionTwo;
	}

	/**
	 * Set the new position of player two and fill in that position on the board.
	 *
	 * @param positionTwo The new position to fill.
	 */
	public void setPositionTwo(Position positionTwo) {
		this.positionTwo = positionTwo;
		setPlace(this.positionTwo, 2);
	}

	/**
	 * Check if there is a possible move to make by a player.
	 *
	 * @return True if there are no more moves to make.
	 */
	public boolean finished(int playerNumber) {
		List<Position> possiblePos = new ArrayList<>();

		for (Move move : Move.values()) {
			try {
				if (playerNumber == 1) {
				possiblePos.add(positionOne.move(move));
				} else {

				possiblePos.add(positionTwo.move(move));
				}
			} catch (InvalidMoveException e) {
				//Normally it is bad practise to leave a catch
				// 		empty, but for now this is fine.
			}

		}
		return possiblePos.stream().allMatch(pos -> getPlace(pos.getRow(), pos.getColumn()) != 0);
	}

	/**
	 * Count the points of the player with player number playerNumber
	 *
	 * @param playerNumber The player number
	 * @return The amount of point of that player
	 */
	public int getPoints(int playerNumber) {
		int points = 0;
		for (int i = 0; i < Board.ROWS; i++) {
			for (int j = 0; j < Board.COLUMNS; j++) {
				if (places[i][j] == playerNumber) {
					points++;
				}
			}
		}
		return points;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < places.length; i++) {
			for (int j = 0; j < places[i].length; j++) {
				if (positionOne.equals(new Position(i,j))) {
					stringBuilder.append(3);
				} else if (positionTwo.equals(new Position(i,j))) {
					stringBuilder.append(4);
				} else {
					stringBuilder.append(places[i][j]);
				}
				stringBuilder.append(" ");
			}
			stringBuilder.append("\n");
		}
		return stringBuilder.toString();
	}
}