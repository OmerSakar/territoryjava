package territory;

public class InvalidMoveException extends Exception {

	public InvalidMoveException(Move direction, Position position) {
		super("Cannot move " + direction.toString().toLowerCase() + " from " + position);
	}
}
