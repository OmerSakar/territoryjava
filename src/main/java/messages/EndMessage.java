package messages;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;

public class EndMessage extends AbstractMessage {

	@SerializedName("message")
	public final MessageType message = MessageType.END;

	public enum Status {LOSE, DRAW, WIN}

	public final Status status;

	public EndMessage(int playerNumber, int game, Status status) {
		super(playerNumber, game);
		this.status = status;
	}

}
