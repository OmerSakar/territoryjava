package messages;

import com.google.gson.annotations.SerializedName;
import territory.Move;

public class MoveMessage extends AbstractMessage {

	@SerializedName("message")
	public final MessageType message = MessageType.MOVE;

	public final Move direction;

	public MoveMessage(int playerNumber, int game, Move direction) {
		super(playerNumber, game);
		this.direction = direction;
	}
}
