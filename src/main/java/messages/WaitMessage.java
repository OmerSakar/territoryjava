package messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class WaitMessage {

	@SerializedName("message")
	public final MessageType message = MessageType.WAIT;

	public String toJsonString() {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(this);
	}
}
