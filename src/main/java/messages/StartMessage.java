package messages;

import com.google.gson.annotations.SerializedName;

public class StartMessage extends AbstractMessage {

	@SerializedName("message")
	public final MessageType message = MessageType.START;

	public StartMessage(int playerNumber, int game) {
		super(playerNumber, game);
	}
}
