package messages;

import com.google.gson.annotations.SerializedName;

public class BoardMessage extends AbstractMessage{

	@SerializedName("message")
	public final MessageType message = MessageType.BOARD;

	public BoardMessage(int playerNumber, int game) {
		super(playerNumber, game);
	}
}
