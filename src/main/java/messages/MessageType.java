package messages;

/**
 * The types of messages the server/client can send or receive.
 */
public enum MessageType {
	WAIT,
	START,
	MOVE,
	VALID,
	MOVE_OTHER,
	BOARD,
	BOARD_RESPONSE,
	DEAD,
	END
}