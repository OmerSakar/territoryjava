package messages;

import com.google.gson.annotations.SerializedName;
import territory.Move;

public class MoveOtherMessage extends AbstractMessage {

	@SerializedName("message")
	public final MessageType message = MessageType.MOVE_OTHER;

	public final Move direction;

	public MoveOtherMessage(int playerNumber, int game, Move direction) {
		super(playerNumber, game);
		this.direction = direction;
	}
}
