package messages;

import com.google.gson.annotations.SerializedName;

public class DeadMessage extends AbstractMessage {

	@SerializedName("message")
	public final MessageType message = MessageType.DEAD;

	public DeadMessage(int playerNumber, int game) {
		super(playerNumber, game);
	}
}
