package messages;

import com.google.gson.annotations.SerializedName;
import territory.Board;
import territory.Position;

public class BoardResponseMessage extends AbstractMessage {

	@SerializedName("message")
	public final MessageType message = MessageType.BOARD_RESPONSE;

	public final int[][] board;
	public final Position player1Cords;
	public final Position player2Cords;

	public BoardResponseMessage(int playerNumber, int game, Board board) {
		super(playerNumber, game);
		this.board = board.getPlaces();
		player1Cords = board.getPositionOne();
		player2Cords = board.getPositionTwo();
	}
}
