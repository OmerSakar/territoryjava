package messages;

import com.google.gson.annotations.SerializedName;
import territory.Move;

public class ValidMessage extends AbstractMessage {

	@SerializedName("message")
	public final MessageType message = MessageType.VALID;

	public final boolean valid;
	public final Move direction;

	public ValidMessage(int playerNumber, int game, boolean valid, Move direction) {
		super(playerNumber, game);
		this.valid = valid;
		this.direction = direction;
	}
}
