package messages;

import com.google.gson.*;

public abstract class AbstractMessage {

	public final int playerNumber;
	public final int game;

	public AbstractMessage(int playerNumber, int game) {
		this.playerNumber = playerNumber;
		this.game = game;
	}

	/**
	 * Convert the message into a JSON String
	 * @return A String representing this object as a JSON Object
	 */
	public String toJsonString() {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(this);
	}

	/**
	 * Subtract the message type from the input which is possibly a JSON object.
	 * @param input The input to subtract the message from
	 * @return the message type as a String or and empty String if the message type could not be subtracted.
	 */
	public static String getMessageFromJson(String input) {
		try {
			JsonElement element = (new JsonParser()).parse(input);
			if (element instanceof JsonNull || !element.isJsonObject()) {
				System.err.println("[ERROR]\t\t" + element + " not a JSON object.");
				return "";
			}
			return element.getAsJsonObject().get("message").getAsString();
		} catch (JsonSyntaxException e) {
			System.err.println("[ERROR]\t\t input cannot be parsed:\t" + input);
			return "";
		}
	}
}
