package server;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class ClientHandler extends Thread {

	private BufferedReader input;
	private BufferedWriter output;
	private Socket socket;
	private BlockingQueue<String> commandQueue;
	private int playerNumber;
	private boolean dead;

	public ClientHandler(Socket client, int playerNumber) {
		this.playerNumber = playerNumber;
		try {
			this.socket = client;
			this.input = new BufferedReader(new InputStreamReader(
					client.getInputStream()));
			this.output = new BufferedWriter(new OutputStreamWriter(
					client.getOutputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read from input and add the input to the command queue for the game session to process.
	 */
	@Override
	public void run() {
		boolean running = true;
		String line;
		while (running) {
			try {
				if ((line = input.readLine()) != null) {
//					System.out.println("[MESSAGE]\t\tpN " + this.playerNumber + ":" + line);
					this.commandQueue.put(line);
				}
			} catch (IOException | InterruptedException e) {
//				e.printStackTrace();
				running = false;
			}
		}
	}

	/**
	 * Send a message to the client associated with this client handler.
	 * @param message The message to send.
	 */
	public void sendMessage(String message) {
		try {
			this.output.write("\n" + message + "\n");
			this.output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setCommandQueue(BlockingQueue<String> commandQueue) {
		this.commandQueue = commandQueue;
	}

	public int getPlayerNumber() {
		return playerNumber;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public Socket getSocket() {
		return socket;
	}
}

