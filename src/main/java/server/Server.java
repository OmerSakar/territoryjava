package server;

import messages.WaitMessage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;

public class Server extends Thread {

	private List<ClientHandler> lobby;
	private ServerSocket serverSocket;

	/**
	 * Start a ServerSocket to accept clients.
	 * @param serverPort The port to start the server at.
	 * @throws IOException Thrown when serverPort is already bound to another process.
	 */
	public Server(int serverPort) throws IOException {
		try {
			this.serverSocket = new ServerSocket(serverPort);
		} catch (IOException e) {
			System.err.println("[ERROR]\t\tServer with port " + serverPort + " cannot be created\n");
			System.exit(1);
		}

		this.lobby = new ArrayList<>();
	}

	/**
	 * Accept clients and add them to the lobby.
	 * When there are enough players, start a new game session.
	 */
	@Override
	public void run() {
		boolean running = true;
		while (running) {
			try {
				Socket client = serverSocket.accept();
				ClientHandler handler = new ClientHandler(client, lobby.size()+1);
				lobby.add(handler);
				if (lobby.size() == 2) {
					GameSession session = new GameSession(lobby, new LinkedBlockingDeque<>());
					session.start();
					lobby = new ArrayList<>();
				} else {
					handler.sendMessage(new WaitMessage().toJsonString());
				}
			} catch (IOException e) {
				e.printStackTrace();
				running = false;
			}
		}
	}


	public static void main(String[] args) throws IOException {
		int portNumber = 4088;
		if (args.length == 1) {
			try{
				portNumber = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.err.println(args[0] + " is not an integer.");
				System.err.println("Using default port " + portNumber);
			}
		}
		Server server = new Server(portNumber);
		server.start();
		try {
			server.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


}
