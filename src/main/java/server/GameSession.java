package server;

import client.Client;
import com.google.gson.*;
import messages.*;
import territory.Board;
import territory.InvalidMoveException;
import territory.Position;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

public class GameSession extends Thread {

	// A map of players to their client handlers.
	private Map<Integer, ClientHandler> handlers;

	// The generated random id of a the game being played.
	private int game;

	// The board of the game
	private Board board;

	// The queue of commands shared across ClientHandlers
	private BlockingQueue<String> commandQueue;

	/**
	 * Add the client to the game and start the game.
	 * @param clientHandlers A list of the clients to be added to this game.
	 * @param commandQueue   The queue of commands to be processed.
	 */
	public GameSession(List<ClientHandler> clientHandlers, BlockingQueue<String> commandQueue) {
		this.board = new Board();
		this.commandQueue = commandQueue;
		handlers = new HashMap<>();
		clientHandlers.forEach(c -> handlers.put(c.getPlayerNumber(), c));

		this.game = new Random().nextInt();
		for (ClientHandler client : handlers.values()) {
			client.setCommandQueue(commandQueue);
			client.sendMessage(new StartMessage(client.getPlayerNumber(), game).toJsonString());
			client.start();
		}
	}

	/**
	 * Run the game until the player cannot make a move or all players are dead.
	 */
	@Override
	public void run() {
		try {
//			System.out.println(this.board);
			while ((!board.finished(1) && !handlers.get(1).isDead()) ||
					(!board.finished(2) && !handlers.get(2).isDead())) {
				handleInput(commandQueue.take());
//				System.out.println(this.board);
			}
			endGame();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handle the input by validating the JSON and delegating the command to the appropriate method.
	 * @param input The input to process.
	 */
	public void handleInput(String input) {
		String messageType = AbstractMessage.getMessageFromJson(input);
		if (messageType.isEmpty()) {
			return;
		}

		Gson gson = new GsonBuilder().create();
//		System.out.println("[LOG]\t\t" + input);
		switch (MessageType.valueOf(messageType)) {
			case BOARD:
				handleBoard(gson.fromJson(input, BoardMessage.class));
				break;
			case MOVE:
				handleMove(gson.fromJson(input, MoveMessage.class));
				break;
			default:
				System.err.println("[ERROR]\t\tCommand " + messageType + " not found");
				break;
		}
	}

	/**
	 * Handle the MOVE message.
	 * @param message The MOVE message.
	 */
	public void handleMove(MoveMessage message) {
		if (handlers.get(message.playerNumber).isDead()) {
			this.getHandler(message.playerNumber).sendMessage(
					new DeadMessage(message.playerNumber, message.game).toJsonString()
			);
			return;
		}

		Position currentPosition = getPlayerPosition(message.playerNumber);
		Position newPosition;
		try {
			newPosition = currentPosition.move(message.direction);
			int newPlace = this.board.getPlace(newPosition.getRow(), newPosition.getColumn());
			if (newPlace == 0) {
				setPlayerPosition(newPosition, message.playerNumber);

				getHandler(message.playerNumber).sendMessage(
						new ValidMessage(message.playerNumber, message.game, true, message.direction)
								.toJsonString()
				);
				getOtherHandlers(message.playerNumber).forEach(c -> c.sendMessage(
						new MoveOtherMessage(message.playerNumber, message.game, message.direction)
								.toJsonString()
				));
			} else if (newPlace != message.playerNumber) {
				setPlayerPosition(newPosition, message.playerNumber);
				getHandler(message.playerNumber).sendMessage(
						new ValidMessage(message.playerNumber, message.game, true, message.direction)
								.toJsonString()
				);
				getOtherHandlers(message.playerNumber).forEach(c -> c.sendMessage(
						new MoveOtherMessage(message.playerNumber, message.game, message.direction)
								.toJsonString()
				));
				this.getHandler(message.playerNumber).sendMessage(
						new DeadMessage(message.playerNumber, message.game).toJsonString()
				);
				setPlayerDeath(message.playerNumber);
			} else {
				getHandler(message.playerNumber).sendMessage(
						new ValidMessage(message.playerNumber, message.game, false, message.direction)
								.toJsonString()
				);
			}

		} catch (InvalidMoveException e) {
			getHandler(message.playerNumber).sendMessage(
					new ValidMessage(message.playerNumber, message.game, false, message.direction)
							.toJsonString()
			);
		}
	}

	/**
	 * Handle the BOARD message.
	 * @param message The BOARD message
	 */
	public void handleBoard(BoardMessage message) {
		BoardResponseMessage response =
				new BoardResponseMessage(message.playerNumber, message.game, this.getBoard());
		this.getHandler(message.playerNumber).sendMessage(response.toJsonString());
	}

	/**
	 * End the game by sending the end messages to all client handlers.
	 */
	public void endGame() {
		int pointsPlayerOne = board.getPoints(1);
		int pointsPlayerTwo = board.getPoints(2);

		EndMessage endMessagePlayerOne;
		EndMessage endMessagePlayerTwo;

		if (pointsPlayerOne > pointsPlayerTwo) {
			endMessagePlayerOne = new EndMessage(1, this.game, EndMessage.Status.WIN);
			endMessagePlayerTwo = new EndMessage(2, this.game, EndMessage.Status.LOSE);
		} else if (pointsPlayerOne < pointsPlayerTwo) {
			endMessagePlayerOne = new EndMessage(1, this.game, EndMessage.Status.LOSE);
			endMessagePlayerTwo = new EndMessage(2, this.game, EndMessage.Status.WIN);
		} else {
			endMessagePlayerOne = new EndMessage(1, this.game, EndMessage.Status.DRAW);
			endMessagePlayerTwo = new EndMessage(2, this.game, EndMessage.Status.DRAW);
		}


		this.handlers.get(1).sendMessage(endMessagePlayerOne.toJsonString());
		this.handlers.get(2).sendMessage(endMessagePlayerTwo.toJsonString());

		for (ClientHandler handler: this.handlers.values()) {
			try {
				handler.getSocket().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Check if all players are dead.
	 * @return True if all players are dead.
	 */
	public boolean playersDead(){
		return handlers.values().stream().map(ClientHandler::isDead).reduce(true, Boolean::logicalAnd);
	}

	/**
	 * Kill a player.
	 * @param playerNumber The player to kill.
	 */
	public void setPlayerDeath(int playerNumber) {
		handlers.get(playerNumber).setDead(true);
	}

	/**
	 * Get the client handler associated with the player number.
	 * @param playerNumber The player number.
	 * @return The handler associated with the player number.
	 */
	public ClientHandler getHandler(int playerNumber) {
		return handlers.get(playerNumber);
	}

	/**
	 * Get all client handler not associated with the player number.
	 * @param playerNumber The player number.
	 * @return A list of client handlers.
	 */
	public List<ClientHandler> getOtherHandlers(int playerNumber) {
		return handlers.values().stream().filter(c -> c.getPlayerNumber() != playerNumber).collect(Collectors.toList());
	}

	/**
	 * Get the current position of a player.
	 * @param playerNumber The player number.
	 * @return The position of that player.
	 */
	public Position getPlayerPosition(int playerNumber) {
		return (playerNumber == 1) ? board.getPositionOne() : board.getPositionTwo();
	}

	/**
	 * Set the current position of a player.
	 * @param newPosition The new position.
	 * @param playerNumber The player number.
	 */
	public void setPlayerPosition(Position newPosition, int playerNumber) {
		if (playerNumber == 1) {
			board.setPositionOne(newPosition);
		} else if (playerNumber == 2) {
			board.setPositionTwo(newPosition);
		}
	}

	public Board getBoard() {
		return board;
	}

}
