package client;

import com.google.gson.*;
import messages.*;
import server.Server;
import territory.Board;
import territory.InvalidMoveException;
import territory.Position;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Client extends Thread {

	private BufferedReader in;
	private BufferedWriter out;
	private ClientView view;

	private AbstractInputHandler inputHandler;
	private boolean gameRunning;
	private boolean runClient;
	private Board board;
	private int playerNumber;
	private int game;

	public Client(String serverUrl, int portNumber) throws IOException {
		InetAddress server = InetAddress.getByName(serverUrl);
		Socket serverSocket = new Socket(server, portNumber);
		serverSocket.setSoTimeout(1000);
		this.in = new BufferedReader(new InputStreamReader(
				serverSocket.getInputStream()));
		this.out = new BufferedWriter(new OutputStreamWriter(
				serverSocket.getOutputStream()));
		this.board = new Board();
		this.view = new ClientView();
	}

	/**
	 * Read from the input stream of the socket and delegate the input to handleServerMessage.
	 */
	@Override
	public void run() {
		String line;
		runClient = true;
		while (runClient) {
			try {
				if ((line = in.readLine()) != null) {
					handleServerMessage(line);
					//System.out.println("[LOG]\t\t" + line);
				}
			} catch (SocketTimeoutException es) {
				es.getMessage();
			} catch (SocketException ee) {
				System.out.println("[ERROR]\t\tConnection with the server is lost");
				runClient = false;
			} catch (IOException e) {
				System.err.println("[ERROR]\t\tConnection Lost");
				runClient = false;
			}
		}
	}

	/**
	 * Delegate the input to the appropriate methods based on the message type.
	 *
	 * @param input The input to inspect
	 */
	public void handleServerMessage(String input) {
		String messageType = AbstractMessage.getMessageFromJson(input);
		if (messageType.isEmpty()) {
			return;
		}

		Gson gson = new GsonBuilder().create();

		switch (MessageType.valueOf(messageType)) {
			case WAIT:
				handleWait();
				break;
			case START:
				handleStart(gson.fromJson(input, StartMessage.class));
				break;
			case VALID:
				handleValid(gson.fromJson(input, ValidMessage.class));
				break;
			case MOVE_OTHER:
				handleMoveOther(gson.fromJson(input, MoveOtherMessage.class));
				break;
			case BOARD_RESPONSE:
				handleBoardResponse(gson.fromJson(input, BoardResponseMessage.class));
				break;
			case DEAD:
				handleDead(gson.fromJson(input, DeadMessage.class));
				break;
			case END:
				handleEnd(gson.fromJson(input, EndMessage.class));
				break;
			default:
				System.err.println("[ERROR]\t\tCommand " + messageType + " not found");
				break;
		}
	}

	private void handleWait() {
		System.out.println("Waiting for another player.");
	}

	public void handleStart(StartMessage message) {
		this.game = message.game;
		this.playerNumber = message.playerNumber;


//		this.inputHandler = new RealPlayerHandler(playerNumber, game, this.board, this.out);
		this.inputHandler = new RandomAIHandler(playerNumber, game, this.board, this.out);
		this.inputHandler.start();

		this.gameRunning = true;
		this.view.drawBoard(this.board, this.playerNumber);
	}


	public void handleValid(ValidMessage message) {
		if (!gameRunning) {
			return;
		}
		if (message.valid) {
			try {
				setPosition(getPosition().move(message.direction));
				this.view.drawBoard(this.board, this.playerNumber);
			} catch (InvalidMoveException e) {
				//Should never be thrown since the server checks it.
				e.printStackTrace();
			}
		} else {
			System.out.println("Move " + message.direction + " not valid");
		}

	}

	public void handleMoveOther(MoveOtherMessage message) {
		try {
			if (playerNumber == 1) {
				this.board.setPositionTwo(this.board.getPositionTwo().move(message.direction));
			} else {
				this.board.setPositionOne(this.board.getPositionOne().move(message.direction));
			}
			this.view.drawBoard(this.board, playerNumber);
		} catch (InvalidMoveException e) {
			e.printStackTrace();
		}
	}

	public void handleBoardResponse(BoardResponseMessage message) {
		this.board = new Board(message.board, message.player1Cords, message.player2Cords);
		this.view.drawBoard(this.board, this.playerNumber);
	}

	public void handleDead(DeadMessage message) {
		gameRunning = false;
		inputHandler.setGameRunning(false);
		this.view.drawBoard(this.board, this.playerNumber);
		System.out.println("You are dead");
	}

	public void handleEnd(EndMessage message) {
		gameRunning = false;
		inputHandler.setGameRunning(false);
		this.view.drawBoard(this.board, this.playerNumber);
		System.out.println("Game ended: you " + message.status.toString().toLowerCase());
		runClient = false;
	}

	public void sendMessage(String message) {
		try {
			this.out.write("\n" + message + "\n");
			this.out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Position getPosition() {
		return (playerNumber == 1) ? this.board.getPositionOne() : this.board.getPositionTwo();
	}

	public void setPosition(Position newPosition) {
		if (playerNumber == 1) {
			board.setPositionOne(newPosition);
		} else {
			board.setPositionTwo(newPosition);
		}
	}

	public static void main(String[] args) throws IOException {
//		List<Client> clients = new ArrayList<>();
//		for (int i : IntStream.range(0, 10).toArray()) {
//			int portNumber = 4088;
//			if (args.length == 1) {
//				try {
//					portNumber = Integer.parseInt(args[0]);
//				} catch (NumberFormatException e) {
//					System.out.println(args[0] + " is not an integer.");
//					System.out.println("Using default port " + portNumber);
//				}
//			}
//			Client client = new Client("localhost", portNumber);
//			client.start();
//			clients.add(client);
//
//			System.out.println("Done");
//		}
//
//		for (Client client : clients) {
//			try {
//				client.join();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
		//System.err.close();
		int portNumber = 4088;
		if (args.length == 1) {
			try {
				portNumber = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.out.println(args[0] + " is not an integer.");
				System.out.println("Using default port " + portNumber);
			}
		}
		Client client = new Client("localhost", portNumber);
		client.start();

		try {
			client.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}