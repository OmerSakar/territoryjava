package client;

import messages.MoveMessage;
import territory.Board;
import territory.Move;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.util.Scanner;

public class RealPlayerHandler extends AbstractInputHandler {

	InputStream inputStream = System.in;

	public RealPlayerHandler(int playerNumber, int game, Board board, BufferedWriter out) {
		super(playerNumber, game, board, out);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void makeMove() {
		Scanner keyboard = new Scanner(System.in);
		String input = keyboard.nextLine();
		if (input == null) {
			return;
		}
		Move move;
		switch (input) {
			case "w":
				move = Move.UP;
				break;
			case "a":
				move = Move.LEFT;
				break;
			case "s":
				move = Move.DOWN;
				break;
			case "d":
				move = Move.RIGHT;
				break;
			default:
				return;
		}
		sendMessage(new MoveMessage(playerNumber, game, move).toJsonString());
	}
}
