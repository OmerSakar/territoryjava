package client;

import territory.Board;

public class ClientView {

	public static String newlines;

	static {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < 80; i++) {
			stringBuilder.append("\n");
		}
		newlines = stringBuilder.toString();
	}

	/**
	 * Clear the screen.
	 * Since there does not seem to be one way to clear the screen across multiple OSs (i.e. Linux, MacOS and Windows),
	 * 		this seemed the best solution for now.
	 */
	public static void clearScreen() {
		System.out.println(newlines);
	}

	/**
	 * Draw the board.
	 * @param board The board to draw.
	 * @param playerNumber The player number.
	 */
	public void drawBoard(Board board, int playerNumber) {
		clearScreen();
		System.out.println(board);
		System.out.println("You are player " + playerNumber);
	}
}
