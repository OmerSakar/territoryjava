package client;

import messages.MoveMessage;
import territory.Board;
import territory.Move;

import java.io.BufferedWriter;
import java.util.*;

public class RandomAIHandler extends AbstractInputHandler {

	private static final Random random = new Random();

	public RandomAIHandler(int playerNumber, int game, Board board, BufferedWriter out) {
		super(playerNumber, game, board, out);
	}

	public void makeMove() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		List<String> possibleInput = Arrays.asList("w", "a", "s", "d");
		String input = possibleInput.get(random.nextInt(possibleInput.size()));

		Move move;
		switch (input) {
			case "w":
				move = Move.UP;
				break;
			case "a":
				move = Move.LEFT;
				break;
			case "s":
				move = Move.DOWN;
				break;
			case "d":
				move = Move.RIGHT;
				break;
			default:
				return;
		}
		sendMessage(new MoveMessage(playerNumber, game, move).toJsonString());
	}



}

