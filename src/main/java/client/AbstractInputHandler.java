package client;

import territory.Board;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * A class to handle the input from the user (be it a real user or a random AI bot).
 */
public abstract class AbstractInputHandler extends Thread {


	protected BufferedWriter output;
	protected boolean gameRunning;
	protected int playerNumber;
	protected int game;
	protected Board board;

	public AbstractInputHandler(int playerNumber, int game, Board board, BufferedWriter out) {
		this.board = board;
		this.playerNumber = playerNumber;
		this.game = game;
		this.output = out;
		this.gameRunning = gameRunning = true;
	}

	/**
	 * By default ask input from the user.
	 */
	@Override
	public void run() {
		while (gameRunning) {
				makeMove();
		}

	}

	/**
	 * Make a move based on the input.
	 */
	public abstract void makeMove();

	/**
	 * Send a message to the player.
	 * @param message
	 */
	public void sendMessage(String message) {
		try {
			this.output.write("\n" + message + "\n");
			this.output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setGameRunning(boolean gameRunning) {
		this.gameRunning = gameRunning;
	}
}


