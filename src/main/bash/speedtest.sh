#!/usr/bin/env bash
CLIENTS=10
PIDS=()


# Start server
java -jar territory-server.jar &
sleep 1

# Start all client
for i in $(seq 1 $CLIENTS); do
	java -jar territory-client.jar &
	PIDS[$i]=$!
done
echo asdf

# Wait for clients to finish
for pid in ${PIDS[*]}; do
	echo $pid
	wait $pid
done

# Kill server
pkill -f territory-server